
var Handlebars = require('handlebars');

module.exports = function (component, context) {
    context = Object.assign({}, require('Components/' + component + '/index').defaults, context);
    component = require('Components/' + component + '/index.hbs');
    return new Handlebars.SafeString(component(context));
};
