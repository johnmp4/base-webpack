
export const defaults = {
    message: "World! (click me)",
    helloClass: "hello"
};

export default function (el, data) {
    require('./button.css');
    el.addEventListener("click", (event) => {
        alert("Hello " + data.message);
    });
}
