
var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var prod = process.argv.indexOf('-p') !== -1;
var assets = /\.(jpe?g|png|gif|svg|eot|ttf|woff2?)$/;

module.exports = {
    entry: './src',
    output: {
        path: path.resolve(__dirname, 'static'),
        filename: '[name].js',
        publicPath: '/'
    },
    resolve: {
        alias: {
            Assets: path.resolve(__dirname, 'src/assets'),
            Components: path.resolve(__dirname, 'src/components'),
            Partials: path.resolve(__dirname, 'src/partials'),
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: { presets: ['env'] }
                }
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            },
            {
                test: assets,
                loader: 'url-loader',
                options: {
                    limit: 1000,
                    name: 'assets/[name].[hash].[ext]'
                }
            },
            {
                test: /\.htaccess$/,
                loader: 'file-loader',
                options: { name: '.htaccess' }
            },
            {
                test: /\.hbs$/,
                loader: 'handlebars-loader',
                options: { inlineRequires: assets }
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [ 'css-loader', 'sass-loader' ]
                })
            },
        ]
    },
    plugins: [
        new ExtractTextPlugin('[name].css'),
        new HtmlWebpackPlugin({
            title: 'Default Page',
            template: './src/index.hbs'
        }),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': prod ? `"production"` : `""`
            }
        }),
    ]
};
